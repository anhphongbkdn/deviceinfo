package app.adz.deviceinfo;

import android.app.ActivityManager;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.Objects;

public class MainActivity extends AppCompatActivity {

    ListView listView;
    ArrayAdapter<String> adapter;
    ArrayList<String> arrayList;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_about) {
            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
            builder.setMessage("Check DeviceInfo by ADZ.");
            builder.setTitle("About the App");
            builder.setNegativeButton("Done", (DialogInterface.OnClickListener) (dialog, which) -> dialog.cancel());
            AlertDialog alertDialog = builder.create();
            alertDialog.show();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setMessage("Do you want to exit the app?");
        builder.setTitle("Warning!");
        builder.setCancelable(false);
        builder.setPositiveButton("Yes", (DialogInterface.OnClickListener) (dialog, which) -> finish());
        builder.setNegativeButton("No", (DialogInterface.OnClickListener) (dialog, which) -> dialog.cancel());
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Context context = getApplicationContext();
        ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        ActivityManager.MemoryInfo memoryInfo = new ActivityManager.MemoryInfo();
        activityManager.getMemoryInfo(memoryInfo);
        long totalMemory = memoryInfo.totalMem / (1024 * 1024);
        listView = findViewById(R.id.listView);
        arrayList = new ArrayList<>();
        adapter = new ArrayAdapter<>(getApplicationContext(), R.layout.lists, arrayList);
        listView.setAdapter(adapter);
        arrayList.add("Serial: " + DeviceUtils.getSerialNumber());
        arrayList.add("Manufacturer: " + Build.MANUFACTURER);
        arrayList.add("Model: " + Build.MODEL);
        arrayList.add("Hardware: " + Build.HARDWARE);
        if (!Objects.equals(Build.BOOTLOADER, "unknown")) {
            arrayList.add("Bootloader: " + Build.BOOTLOADER);
        }
        if (!Objects.equals(Build.BOARD, "unknown")) {
            arrayList.add("Board: " + Build.BOARD);
        }
        if (Build.VERSION.SDK_INT >= 31) {
            if (!Objects.equals(Build.SOC_MANUFACTURER, "unknown")) {
                arrayList.add("SoC Manufacturer: " + Build.SOC_MANUFACTURER);
            }
            if (!Objects.equals(Build.SOC_MODEL, "unknown")) {
                arrayList.add("SoC Model: " + Build.SOC_MODEL);
            }
        }
        arrayList.add("Release: " + Build.VERSION.RELEASE);
        arrayList.add("Patch: " + (Build.VERSION.SECURITY_PATCH));
        arrayList.add("SDK_INT: " + Build.VERSION.SDK_INT);
        arrayList.add("Fingerprint: " + Build.FINGERPRINT);
        arrayList.add("ID: " + Build.ID);
        arrayList.add("Type: " + Build.TYPE);
        arrayList.add("RAM: " + totalMemory + "MB");

        adapter.notifyDataSetChanged();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            int i;

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                i = i + 1;
                if (i >= 5 && i % 10 == 0) {
                    Toast.makeText(MainActivity.this, "Don't be a fool to tap continuously!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}